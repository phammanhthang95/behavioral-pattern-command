package pkg

type OffCommand struct {
	Device Device
}

func (o *OffCommand) execute() {
	o.Device.off()
}

// ConcreteCommand : là các implementation của Command. Định nghĩa một sự gắn kết giữa một đối tượng Receiver và một hành động. Thực thi execute() bằng việc gọi operation đang hoãn trên Receiver. Mỗi một ConcreteCommand sẽ phục vụ cho một case request riêng.