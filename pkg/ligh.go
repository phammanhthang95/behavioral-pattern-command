package pkg

import "fmt"

type Light struct {
	isRunning bool
}

func (t *Light) on() {
	t.isRunning = true
	fmt.Println("Turn on")
}

func (t *Light) off() {
	t.isRunning = false
	fmt.Println("Turn off")
}

// Concrete Receiver: implement từ device