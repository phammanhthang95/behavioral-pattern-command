package pkg

type Button struct {
	Command Command
}

func (b *Button) Press() {
	b.Command.execute()
}

// Invoker : tiếp nhận ConcreteCommand từ Client và gọi execute() của ConcreteCommand để thực thi request.