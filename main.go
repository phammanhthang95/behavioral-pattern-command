package main

import "behavioral-pattern-command/pkg"

func main() {
	light := &pkg.Light{}
	onCommand := &pkg.OnCommand{Device: light}
	offCommand := &pkg.OffCommand{Device: light}
	onButton := &pkg.Button{
		Command: onCommand,
	}
	onButton.Press()
	offButton := &pkg.Button{
		Command: offCommand,
	}
	offButton.Press()
}
